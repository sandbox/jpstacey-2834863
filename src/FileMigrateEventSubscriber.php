<?php

namespace Drupal\media_entity_upgrade;

use Drupal\migrate\Event\MigrateEvents;
use Drupal\migrate\Event\MigratePostRowSaveEvent;
use Drupal\Core\Entity\EntityManagerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Create a media entity whenever a file is migrated.
 */
class FileMigrateEventSubscriber implements EventSubscriberInterface {

  /**
   * @var X
   *   X provided by the factory injected below in the constructor.
   */
  protected $entityManager;

  /**
   * Implements __construct().
   *
   * Dependency injection defined in services.yml.
   */
  public function __construct(EntityManagerInterface $entityManager) {
    $this->entityManager = $entityManager;
  }

  /**
   * {@inheritDoc}
   */
  public static function getSubscribedEvents() {
    return [MigrateEvents::POST_ROW_SAVE => [['onPostRowSave']]];
  }

  /**
   * Subscribed event callback: MigrateEvents::POST_ROW_SAVE.
   *
   * If the saved entity is a file, create a media entity to wrap around it.
   *
   * @param MigratePostRowSaveEvent $event
   *   The event triggered.
   */
  public function onPostRowSave(MigratePostRowSaveEvent $event) {
    if ($event->getMigration()->getPluginId() != 'd6_file') {
      return;
    }

    // Create 'document' media for all files of unknown type.
    $bundle = 'document';
    // Check if MIME type is known, and change media bundle accordingly.
    $destinationIds = $event->getDestinationIdValues();
    $file = $this->entityManager->getStorage('file')->load($destinationIds[0]);
    if (strpos($file->getMimeType(), 'image/') === 0) {
      $bundle = 'image';
    }

    // Create media entity and save.
    $media = $this->entityManager->getStorage('media')->create([
      'bundle' => $bundle,
      'uid' => $file->uid,
      'name' => $file->name,
      'field_media_file' => [
        'target_id' => $destinationIds[0]
      ],
    ]);
    $media->save();
  }

}
